# -*- encoding=utf8 -*-
__author__ = "fanxiaohao"

import json
import multiprocessing

from airtest.core.api import *
from airtest.core.android.recorder import *
from airtest.core.android.adb import *

auto_setup(__file__)

from tools.mode import poco
from tools.helper import exists_any, Recorder_device, Confing
from tools.decorator import retry

path = os.path.abspath(os.path.dirname(__file__))
l_path = os.path.abspath(os.path.dirname(path))

class TalkingTom():
    def __init__(self):
        self.game = Game()
        self.con = Confing()
        self.recorder = Recorder_device()
        self.recorder.start_recorder(max_time=1800)
        self.packagename = self.con.get_config('JK', 'packagename')
        self.channel = self.con.get_config('JK', 'channel')
        self.apk_path = self.con.get_config('JK', 'apk_path')
        self.path = path
        self.l_path = l_path

    def skip_useragreement(self):
        '''
        用户协议界面
        :return:
        '''
        print("【正在执行---skip_useragreement()】")
        sleep(3)
        pos = exists_any(Template(r"tpl1616745256133.png", record_pos=(-0.002, 0.031), resolution=(1080, 2340)))
        pos1 = exists_any([
            Template(r"tpl1616745564862.png", record_pos=(0.143, -0.105), resolution=(1080, 2340)),
            Template(r"tpl1617160583572.png", record_pos=(0.227, -0.041), resolution=(1080, 2340))
        ])
        if pos != False :
            touch(pos1)
        close = exists_any(Template(r"tpl1616745637664.png", record_pos=(0.438, -0.952), resolution=(1080, 2340)))
        if pos != False :
            touch(close)
        sleep(2)
        agree_bt = exists_any(Template(r"tpl1616745987159.png", record_pos=(0.219, 0.409), resolution=(1080, 2340)))
        if agree_bt != False :
            touch(agree_bt)

    def skip_permission(self, times=2):
        '''
        跳过权限
        :param times: 循环跳过次数
        :return:
        '''
        print("【正在执行---skip_permission()】")
        sleep(3)
        permission_bt = [
            poco(text="始终允许"),
            poco(text="允许"),
            poco(text="确定"),
            poco(text="仅在使用中允许")
        ]
        for i in range(0, times):
            pos1 = exists_any(permission_bt)
            if pos1 != False:
                touch(pos1)
                print("第{}次权限申请成功".format(i+1))
                sleep(0.5)

    @retry
    def age_choice(self):
        '''
        年龄选择界面
        :return:
        '''
        print("【正在执行---age_choice()】")
        sleep(5)
        age = exists_any([
            Template(r"tpl1616751741575.png", record_pos=(0.003, 0.033), resolution=(1080, 2340)),
            Template(r"tpl1618989193812.png", record_pos=(0.006, -0.008), resolution=(720, 1600))
        ])
        if age == False:
            print('手动抛出异常')
            raise
        swipe(
            Template(r"tpl1618989942078.png", record_pos=(-0.004, 0.086), resolution=(720, 1600)),
            Template(r"tpl1618989959934.png", record_pos=(-0.007, -0.113), resolution=(720, 1600))
        )
        sleep(1)
        ok_bt = exists_any([
            # Template(r"tpl1616752315923.png", record_pos=(-0.001, 0.417), resolution=(1080, 2340)),
            # Template(r"tpl1618989232994.png", record_pos=(0.007, 0.375), resolution=(720, 1600))
            poco(nameMatches='(.*)id/sharingAgeScreeningButtonOk')
        ])
        if ok_bt != False:
            touch(ok_bt)
            sleep(3)
        poc = exists_any([
            poco(text='允许'),
            poco(text="仅在使用中允许"),
            poco(text='确定'),
            poco(text='始终允许'),
        ])
        if poc != False:
            touch(poc)

    @retry
    def setting(self):
        '''
        设置界面
        :return:
        '''
        sleep(5)
        print("【正在执行---setting()】")
        setting_bt = exists_any([
            # Template(r"tpl1616839206002.png", record_pos=(0.203, -0.733), resolution=(1080, 2340)),
            # Template(r"tpl1618991371973.png", record_pos=(0.425, -1.007), resolution=(720, 1600))
            poco(nameMatches='(.*)id/buttonInfo')
        ])
        if setting_bt != False:
            touch(setting_bt)
            sleep(2)

        setting_bt2 = exists_any([
            poco(nameMatches='(.*)id/infoWebButtonMoreSettings'),
            # Template(r"tpl1616840159528.png", record_pos=(-0.002, -0.27), resolution=(1080, 2340)),
            # Template(r"tpl1618991418929.png", record_pos=(0.022, -0.438), resolution=(720, 1600)),
        ])
        touch(setting_bt2)
        sleep(3)

        back_bt = exists_any([
            poco(name='向上导航'),
            Template(r"tpl1616840201560.png", record_pos=(-0.419, -0.928), resolution=(1080, 2340)),
            Template(r"tpl1618991489441.png", record_pos=(-0.419, -0.961), resolution=(720, 1600))
        ])
        if back_bt != False:
            touch(back_bt)
            sleep(3)
            self.check_reward()

        sleep(3)
        setting_bt = exists_any([
            poco(nameMatches='(.*)id/buttonInfo'),
            # Template(r"tpl1616839206002.png", record_pos=(0.203, -0.733), resolution=(1080, 2340)),
            # Template(r"tpl1618991371973.png", record_pos=(0.425, -1.007), resolution=(720, 1600))
        ])
        if setting_bt != False:
            touch(setting_bt)
            sleep(2)

        game_show = exists_any([
            poco(text='游戏说明'),
            Template(r"tpl1618991657960.png", record_pos=(0.013, 0.249), resolution=(720, 1600))
        ])
        if game_show != False:
            touch(game_show)
            sleep(2)

        close_bt = exists_any([
            poco(nameMatches='(.*)id/infoWebButtonClose'),
            Template(r"tpl1616839408095.png", record_pos=(0.428, -0.77), resolution=(1080, 2340))
        ])
        # print("正在退出设置界面")
        touch(close_bt)
        self.check_reward()

        sleep(3)
        setting_bt = exists_any([
            poco(nameMatches='(.*)id/buttonInfo'),
            Template(r"tpl1616839206002.png", record_pos=(0.203, -0.733), resolution=(1080, 2340)),
            Template(r"tpl1618991371973.png", record_pos=(0.425, -1.007), resolution=(720, 1600))
        ])
        touch(setting_bt)
        sleep(2)

        clause = exists_any([
            poco(nameMatches='(.*)id/infoWebLinkEulaAndContact'),
            Template(r"tpl1617007988859.png", record_pos=(0.019, 1.007), resolution=(1080, 2340))
            ])
        touch(clause)
        sleep(2)

        close_bt = exists_any([
            poco(nameMatches='(.*)id/infoWebButtonClose'),
            Template(r"tpl1616839408095.png", record_pos=(0.428, -0.77), resolution=(1080, 2340))])
        touch(close_bt)
        sleep(2)
        self.check_reward()

    @retry
    def video(self):
        '''
        录像机
        :return:
        '''
        sleep(5)
        print("【正在执行---video()】")
        video_bt = exists_any([
            Template(r"tpl1617008581838.png", record_pos=(-0.406, -0.92), resolution=(1080, 2340)),
            poco(nameMatches='(.*)id/recorderButton')
        ])
        touch(video_bt)
        sleep(5)
        touch(video_bt)

        sleep(3)
        play_bt = exists_any([
            poco(text='播放'),
            Template(r"tpl1617008742507.png", record_pos=(-0.176, -0.473), resolution=(1080, 2340))
        ])
        touch(play_bt)
        sleep(7)

        # stop_bt = exists_any(Template(r"tpl1617008827855.png", record_pos=(-0.409, -0.925), resolution=(1080, 2340)))
        # touch(stop_bt)

        close_bt = exists_any(
            # Template(r"tpl1616839408095.png", record_pos=(0.428, -0.77), resolution=(1080, 2340))
            poco(nameMatches='(.*)id/recorderMenuButtonClose')
        )

        touch(close_bt)
        sleep(2)
        self.check_reward()

    @retry
    def cross_promote(self):
        '''
        交叉推广
        :return:
        '''
        sleep(5)
        print("【正在执行---cross_promote()】")
        for i in range(2):
            promote_bt = exists_any([
                Template(r"tpl1617009010577.png", record_pos=(0.396, -0.756), resolution=(1080, 2340)),
                Template(r"tpl1617009027036.png", record_pos=(0.395, -0.763), resolution=(1080, 2340)),
            ])
            if promote_bt != False:
                touch(promote_bt)

            close_bt = exists_any(Template(r"tpl1617009498086.png", record_pos=(0.434, -0.949), resolution=(1080, 2340)))
            if close_bt != False:
                touch(close_bt)
                sleep(2)
            self.check_reward()

    def check_reward(self):
        print("检测视频广告入口")
        reward_bt = exists_any(Template(r"tpl1617778388857.png", record_pos=(-0.157, 0.164), resolution=(1080, 2340)))
        if reward_bt != False:
            touch(reward_bt)
            sleep(40)
            keyevent("KEYCODE_BACK")


    @retry
    def function_button(self):
        '''
        主界面功能按钮
        :return:
        '''
        sleep(5)
        print("【正在执行---function_button()】")
        fart_bt = exists_any([
            poco(nameMatches='(.*)id/gasmask'),
            Template(r"tpl1617009694953.png", record_pos=(-0.402, 0.744), resolution=(1080, 2340))
        ])
        touch(fart_bt)
        sleep(2)

        claw_bt = exists_any([
            poco(nameMatches='(.*)id/pawn'),
            Template(r"tpl1617009946874.png", record_pos=(-0.406, 0.932), resolution=(1080, 2340))
        ])
        touch(claw_bt)
        sleep(2)

        food_bt = exists_any([
            poco(nameMatches='(.*)id/foodButton'),
            Template(r"tpl1617010024776.png", record_pos=(0.403, 0.935), resolution=(1080, 2340))
        ])
        touch(food_bt)
        sleep(2)

        pepper = exists_any([
            poco(nameMatches='(.*)id/foodItemChilly'),
            Template(r"tpl1617010075805.png", record_pos=(0.403, 0.118), resolution=(1080, 2340))
        ])
        if pepper != False:
            touch(pepper)
            sleep(20)

        cake = exists_any([
            poco(nameMatches='(.*)id/foodItemCake'),
            Template(r"tpl1617010192715.png", record_pos=(0.404, 0.524), resolution=(1080, 2340))
        ])
        if cake != False:
            touch(cake)
        sleep(20)

        tom = exists_any(Template(r"tpl1617778737937.png", record_pos=(0.019, 0.103), resolution=(1080, 2340)))
        if tom != False:
            for i in range(2):
                reward = exists_any(Template(r"tpl1617778388857.png", record_pos=(-0.157, 0.164), resolution=(1080, 2340)))
                if reward != False:
                    self.check_reward()
                    break
                else:
                    sleep(30)

    def process(self):
        '''
        流程
        :return:
        '''
        self.setting()
        self.video()
        self.cross_promote()
        self.function_button()

    def first(self):
        '''
        首次启动需执行
        :return:
        '''
        self.skip_useragreement()
        self.skip_permission()
        self.age_choice()

    @retry
    def quit_game(self):
        '''
        退出游戏
        :return:
        '''
        print("正在手动退出游戏....")
        keyevent("BACK")
        quit_bt = exists_any([
            poco(text="退出游戏"),
            poco(text='退出'),
            poco(text='结束游戏'),
            poco(text='确定退出')
        ])
        if quit_bt != False:
            touch(quit_bt)
        else:
            keyevent('BACK')

    def snap(self):
        while True:
            t = time.strftime("%Y%m%H%M%S", time.localtime())
            mag_path = os.path.join(self.l_path, r'resultlog\{}\image\{}.jpg').format(self.channel, t)
            snapshot(filename=mag_path)
            time.sleep(3)

    def cover_install(self):
        '''
        覆盖安装
        :return:
        '''
        install(self.apk_path)
        print("正在进行覆盖安装...")
        self.game.start_app(self.packagename)
        self.process()
        self.game.stop_app(self.packagename)

    def process_run(self, num):
        self.game.wake_phone()
        self.game.start_app(self.packagename)
        self.first()
        self.process()
        for i in range(num):
            self.quit_game()
            self.game.start_app(self.packagename)
            self.process()
        self.game.stop_app(self.packagename)

        # 覆盖安装
        self.cover_install()

        # data = json.dumps(self.result_game, indent=4,ensure_ascii=False, sort_keys=False,separators=(',', ':'))
        # print(data)
        self.recorder.stop_recorder(channel=self.channel)


class Game():
    '''
    游戏基础操作
    '''
    def start_app(self, packagename):
        print("正在启动游戏....")
        start_app(packagename)
        sleep(5)

    def stop_app(self, packagename):
        print('正在关闭游戏...')
        stop_app(packagename)

    def clear_app(self, packagename):
        print('正在清除数据...')
        clear_app(packagename)
        sleep(5)

    def wake_phone(self):
        wake()
        sleep(2)


def run_game():
    tt = TalkingTom()
    tt.process_run(2)
    # t1 = threading.Thread(target=tt.process_run,args=(2,))
    # t2 = threading.Thread(target=tt.snap)
    # t2.setDaemon(True)
    # t2.start()
    # t1.start()
    # t1.join()
    # tt.setting()
    # tt.age_choice()




