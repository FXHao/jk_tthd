# -*- encoding=utf8 -*-
__author__ = "fanxiaohao"

from airtest.core.api import *


auto_setup(__file__)


from tools.mode import poco
from tools.helper import exists_any, snapshot_mag
from tools.decorator import retry
import threading

class Xiaomi():
    def __init__(self):
        self.poco = poco
        self.banner = None
        self.interstitial = None
        self.video = None

    def login(self):
        pass

    def getAdPoco(self):
        self.banner = {
            'banner': self.poco("android.widget.LinearLayout").offspring("android:id/content").offspring("com.outfit7.talkingtom:id/activead").child("android.widget.FrameLayout").child("android.widget.FrameLayout").offspring("android.widget.RelativeLayout"),
            'close': self.poco("android.widget.LinearLayout").offspring("android:id/content").offspring("com.outfit7.talkingtom:id/activead").child("android.widget.FrameLayout").child("android.widget.FrameLayout").offspring("android.widget.RelativeLayout").child("android.widget.FrameLayout")[0].child("android.widget.ImageView")
        }

        self.interstitial = {
            'interstitial': self.poco("android.widget.FrameLayout").child("android.widget.FrameLayout").child("android.widget.FrameLayout").child("android.widget.FrameLayout").child("android.widget.FrameLayout").child("android.widget.RelativeLayout").child("android.widget.RelativeLayout"),
            'close': None
        }
        return self.banner, self.interstitial



