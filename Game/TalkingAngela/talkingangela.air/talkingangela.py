# -*- encoding=utf8 -*-
__author__ = "fanxiaohao"

import json
import multiprocessing

from airtest.core.api import *
from airtest.core.android.recorder import *
from airtest.core.android.adb import *

auto_setup(__file__)

from tools.mode import poco
from tools.helper import exists_any, Recorder_device, Confing
from tools.decorator import retry
import threading

path = os.path.abspath(os.path.dirname(__file__))
l_path = os.path.abspath(os.path.dirname(path))


class TalkingAngela():
    def __init__(self):
        self.game = Game()
        self.con = Confing()
        self.recorder = Recorder_device()
        self.result_game = {
            '用户协议界面': 'error',
            '权限申请':'error',
            '年龄选择': 'error',
            '设置界面': 'error',
            '录像': 'error',
            '交叉推广': 'error',
            '主界面功能按钮': 'error',
            '视频广告': 'error'
        }
        self.recorder.start_recorder(max_time=1800)
        self.packagename = self.con.get_config('JK', 'packagename')
        self.channel = self.con.get_config('JK', 'channel')
        self.apk_path = self.con.get_config('JK', 'apk_path')
        self.path = path
        self.l_path = l_path

    def skip_useragreement(self):
        '''
        用户协议界面
        :return:
        '''
        print("【正在执行---skip_useragreement()】")
        sleep(3)
        pos = exists_any(Template(r"tpl1617965197424.png", record_pos=(0.003, -0.398), resolution=(1080, 2340)))
        pos1 = exists_any([
            Template(r"tpl1616745564862.png", record_pos=(0.143, -0.105), resolution=(1080, 2340)),
            Template(r"tpl1617160583572.png", record_pos=(0.227, -0.041), resolution=(1080, 2340))
        ])
        if pos != False :
            touch(pos1)
        close = exists_any(Template(r"tpl1616745637664.png", record_pos=(0.438, -0.952), resolution=(1080, 2340)))
        if pos != False :
            touch(close)
        sleep(2)
        agree_bt = exists_any(Template(r"tpl1616745987159.png", record_pos=(0.219, 0.409), resolution=(1080, 2340)))
        if agree_bt != False :
            touch(agree_bt)
            self.result_game['用户协议界面']='pass'
            print('用户协议界面--pass')

    def skip_permission(self, times=2):
        '''
        跳过权限
        :param times: 循环跳过次数
        :return:
        '''
        print("【正在执行---skip_permission()】")
        sleep(3)
        permission_bt = [
            poco(text="始终允许"),
            poco(text="允许"),
            poco(text="确定"),
            poco(text="仅在使用中允许")
        ]
        for i in range(0, times):
            pos1 = exists_any(permission_bt)
            if pos1 != False:
                touch(pos1)
                print("第{}次权限申请成功".format(i+1))
                sleep(0.5)
        self.result_game['权限申请'] = 'pass'
        print('权限选择--pass')

    @retry
    def age_choice(self):
        '''
        年龄选择
        :return:
        '''
        sleep(5)
        print("【正在执行---age_choice()】")
        # age_bt = exists_any(Template(r"tpl1617965365629.png", record_pos=(0.012, -0.161), resolution=(1080, 2340)))
        ch_bt = exists_any(Template(r"tpl1617965413916.png", record_pos=(0.339, 0.191), resolution=(1080, 2340)))
        touch(ch_bt)
        ok_bt = exists_any(Template(r"tpl1617965493786.png", record_pos=(-0.006, 0.759), resolution=(1080, 2340)))
        touch(ok_bt)




class Game():
    '''
    游戏基础操作
    '''
    def start_app(self, packagename):
        print("正在启动游戏....")
        start_app(packagename)
        sleep(5)

    def stop_app(self, packagename):
        print('正在关闭游戏...')
        stop_app(packagename)

    def clear_app(self, packagename):
        print('正在清除数据...')
        clear_app(packagename)
        sleep(5)

    def wake_phone(self):
        wake()
        sleep(2)


def run_game():
    pass