# -*- coding: utf-8 -*-
__author__ = "fanxiaohao"

from airtest.core.api import *

auto_setup(__file__)

path = os.path.abspath(os.path.dirname(__file__))
l_path = os.path.abspath(os.path.dirname(path))

from tools.helper import install_app, get_apk_list, Confing
from airtest.core.android.adb import *
from airtest.core.android.android import *
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
from tools.mode import poco
from airtest.core.settings import Settings as ST
import threading


apk_path_list = []



def ta():
    air_path = os.path.join(path, r"Game\TalkingAngela\talkingangela.air")
    # sys.path.append(air_path)
    # using("talkingangela.air")
    using(air_path)
    from talkingangela import run_game
    run_game()

def tt():
    air_path = os.path.join(path, r"Game\TalkingTom\talkingtom.air")
    # sys.path.append(air_path)
    # using("talkingtom.air")
    using(air_path)
    from talkingtom import run_game
    run_game()


functions = {
    'TalkingTom': tt,
    'TalkingAngela': ta
}

def run():
    apk = get_apk_list(apk_path_list)
    print(apk)
    for i in apk_path_list:
        install_app(i)
        game_name = Confing().get_config('JK', 'game')
        functions[game_name]()

def snap():
    while True:
        game_name = Confing().get_config('JK', 'game')
        channel = Confing().get_config('JK', 'channel')
        t = time.strftime("%Y%m%H%M%S", time.localtime())
        mag_path = os.path.join(path, r'Game\{}\resultlog\{}\image\{}.jpg').format(game_name, channel, t)
        snapshot(filename=mag_path)
        time.sleep(3)


if __name__ == '__main__':
    game_r = threading.Thread(target=run)
    sna_r = threading.Thread(target=snap)
    sna_r.setDaemon(True)
    game_r.start()
    sleep(30)
    sna_r.start()
    game_r.join()
