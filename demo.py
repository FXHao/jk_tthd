# -*- coding: utf-8 -*-

def q(a,s):
    try:
        for i in a[s:]:
            s+=1
            print('%.2f'%(100/i))
    except:
        print("分母为{}，发生错误！".format(i))
        return q(a,s)


def sumli(li, target):
    for i in range(0, len(li)):
        for j in range(0, len(li)):
            if i == j:
                continue
            if li[i] + li[j] == target:
                return [i,j]


def addTwoNumbers(l1, l2):
    l3 = []
    for i in range(0, len(l1)):
        l3.append(str(l1[len(l1)-i-1]))
    l1num = int(''.join(l3))
    l3.clear()
    for j in range(0, len(l2)):
        l3.append(str(l2[len(l2)-j-1]))
    l2num = int(''.join(l3))
    print(l1num,l2num)
    suml = l1num + l2num
    l = [ int(item) for item in str(suml) ]
    l.reverse()
    return l


def lengthOfLongestSubstring(s):
    if len(s) <= 1:
        return len(s)
    y = 0
    x = ''
    for i in range(len(s)):
        if s[i] not in x:
            x = x + s[i]
        else:
            x = x + s[i]
            listx = list(x)
            del listx[:listx.index(s[i])+1]
            x = str(''.join(listx))
        print(x)
        if len(x) > y:
            y = len(x)
    return y


l1 = [1,3,4]
l2 = [2]
def findMedianSortedArrays(nums1, nums2):
    data = nums1 + nums2
    data.sort()
    print(data)
    size = len(data)
    if size % 2 == 0:
        return (data[size//2] + data[(size//2)-1])/2
    return data[(size-1)//2]


def longestPalindrome(s: str):
    '''
    第5题 给你一个字符串 s，找到 s 中最长的回文子串。
    :param s:
    :return:
    '''
    if (len(s) < 2):
        return s
    if (s == s[::-1]):
        return s
    maxstr = ''
    aa = []
    for j in s:
        if j in aa:
            continue
        aa.append(j)
        index = [i for i,x in enumerate(s) if x == j]
        for index_a in index:
            for index_b in index:
                tag = s[index_a: index_b+1]
                if tag == tag[::-1]:
                    if len(tag) > len(maxstr):
                        maxstr = tag
    return maxstr


def reverse(x: int) -> int:
    '''
    给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。
    如果反转后整数超过 32 位的有符号整数的范围 [−2**31,  2**31 − 1] ，就返回 0。
    :param self:
    :param x:
    :return:
    '''
    if x <= 0:
        x *= -1
        tag = -int(str(x)[::-1])
        return tag if -2**31 <= tag else 0
    tag = int(str(x)[::-1])
    return tag if tag <= 2**31-1 else 0


def myAtoi(s: str):
    '''
    函数 myAtoi(string s) 的算法如下：
    读入字符串并丢弃无用的前导空格
    检查下一个字符（假设还未到字符末尾）为正还是负号，读取该字符（如果有）。 确定最终结果是负数还是正数。 如果两者都不存在，则假定结果为正。
    读入下一个字符，直到到达下一个非数字字符或到达输入的结尾。字符串的其余部分将被忽略。
    将前面步骤读入的这些数字转换为整数（即，"123" -> 123， "0032" -> 32）。如果没有读入数字，则整数为 0 。必要时更改符号（从步骤 2 开始）。
    如果整数数超过 32 位有符号整数范围 [−231,  231 − 1] ，需要截断这个整数，使其保持在这个范围内。具体来说，小于 −231 的整数应该被固定为 −231 ，大于 231 − 1 的整数应该被固定为 231 − 1 。
    返回整数作为最终结果。
    :param self:
    :param s:
    :return:
    '''
    x = ''
    sgin = ''
    tag = False
    for i in s:
        try:
            i = int(i)
            x = x + str(i)
            tag = True
        except:
            if tag == False:
                if (i == '-') or (i=='+') or (i==' '):
                    sgin = i if (i=='+') or (i=='-') else ''
                    continue
                return 0
            else:
                break
    x = int(sgin + x)
    if x <= -2**31:
        x = -2**31
    if x >= 2**31 - 1:
        x = 2**31 -1
    return x


def intToRoman(num: int):
    Roman = {
        1000: 'M',
        900 : 'CM',
        500 : 'D',
        400 : 'CD',
        100 : 'C',
        90  : 'XC',
        50  : 'L',
        40  : 'XL',
        10  : 'X',
        9   : 'IX',
        5   : 'V',
        4   : 'IV',
        1   : 'I',
    }
    result = ''
    for i in Roman:
        while num >= i:
            result = result + Roman[i]
            num = num - i
    return result

def romanToInt(s: str) -> int:
    Roman = {
        'M' :1000,
        'CM':900,
        'D' :500,
        'CD':400,
        'C' : 100,
        'XC':  90,
        'L' :  50,
        'XL':  40,
        'X' :  10,
        'IX':  9,
        'V' :  5,
        'IV':   4,
        'I' :   1,
    }
    s_list = list(s)
    num = 0
    while len(s_list)>0:
        tag = s_list[0] + s_list[1]
        if tag not in Roman:
            tag = s_list[0]
            num = num + Roman[tag]
            del s_list[0]
            continue
        num = num + Roman[tag]
        del s_list[0:2]
    return num

s = 'III'
print(romanToInt(s))










