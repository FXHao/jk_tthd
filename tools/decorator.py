# -*- coding: utf-8 -*-
__author__ = "fanxiaohao"

from airtest.core.api import *
from tools.mode import poco
from tools.helper import exists_any, snapshot_mag, Recorder_device, Confing
import time
import os

path = os.path.abspath(os.path.dirname(__file__))
l_path = os.path.abspath(os.path.dirname(path))


game_poc = [
    poco(text='下次再说'),
    poco(text='允许'),
    poco(text="仅在使用中允许"),
    poco(text='确定'),
    poco(text='确认'),
    poco(text='跳过'),
    poco(text='再玩一会'),
    poco(text='跳过'),
    poco(text='始终允许'),
    poco(text='取消'),
    Template(r"tpl1618829090189.png", record_pos=(0.332, 0.213), resolution=(1080, 2340)),
    Template(r"tpl1618829328217.png", record_pos=(-0.369, -0.319), resolution=(1080, 2340)),
    poco(nameMatches='(.*)id/tt_insert_dislike_icon_img'),
    Template(r"tpl1619758377563.png", record_pos=(0.308, -0.393), resolution=(1080, 2340))
]

tt_poc = [
    Template(r"tpl1617778101988.png", record_pos=(0.454, -0.96), resolution=(1080, 2340)),
    Template(r"tpl1616755903701.png", record_pos=(-0.219, 0.217), resolution=(1080, 2340)),
    Template(r"tpl1616839932274.png", record_pos=(0.426, -0.873), resolution=(1080, 2340)),
    Template(r"tpl1617011827775.png", record_pos=(0.434, -0.946), resolution=(1080, 2340)),
    Template(r"tpl1618994865178.png", record_pos=(0.425, -0.932), resolution=(1080, 2340))


]

ta_poc = [

]


poco_dict = {
    'TalkingTom': game_poc + tt_poc,
    'TalkingAngela': game_poc + ta_poc
}

def retry(func):
    def run_case_again(*args, **kwargs):
        packagename = Confing().get_config('JK', 'packagename')
        channel = Confing().get_config('JK', 'channel')
        game_name = Confing().get_config('JK', 'game')
        mag_path = os.path.join(l_path, r"Game\{}\resultlog\{}\err").format(game_name, channel)
        for i in range(4):
            try:
                if i >= 3:
                    start_app(packagename)
                    sleep(30)
                func(*args, **kwargs)
                break
            except:
                print("执行异常，正在检测程序是否崩溃...")
                fall()
                snapshot_mag(mag_path, '异常')
                pos = exists_any(poco_dict[game_name])
                if pos != False:
                    touch(pos)
                else:
                    sleep(1)
                    print("点击返回键")
                    keyevent("KEYCODE_BACK")
                    print("尝试重新执行用例...")
    return run_case_again


def fall():
    '''检测APP进程是否存在'''
    app = device()
    recd = Recorder_device()
    packagename = Confing().get_config('JK', 'packagename')
    channel = Confing().get_config('JK', 'channel')
    game_name = Confing().get_config('JK', 'game')
    t = time.strftime("%Y%m%H%M%S", time.localtime())
    try:
        # 判断APP进程是否存在
        str = 'ps|grep ' + packagename
        app.shell(str)
    except:
        # 获取日志
        print("检测游戏进程不存在，获取日志！")
        log_path = os.path.join(l_path, r'Game\{}\resultlog\logcat_{}.txt').format(game_name, t)
        with open(log_path, 'wb') as f:
            for x in app.logcat():
                f.write(x)
        recd.stop_recorder(channel=channel)
        sleep(10)
        return

        # 结束程序
        # print('正在结束脚本运行！')
        # os._exit(0)







