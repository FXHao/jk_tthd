# -*- coding: utf-8 -*-
__author__ = "fanxiaohao"

from airtest.core.api import *

from airtest.core.android.adb import *
from airtest.core.android.android import *
from airtest.core.android.recorder import *

auto_setup(__file__)
from poco.drivers.android.uiautomation import AndroidUiautomationPoco
from poco.proxy import UIObjectProxy
from airtest.core.android.rotation import XYTransformer

from airtest.core.settings import Settings as ST
ST.FIND_TIMEOUT_TMP = 5 # exists等待的时间
# import os
import configparser
from tools.mode import poco
from tools.send_email import Send_Mail
import threading

path = os.path.abspath(os.path.dirname(__file__))
l_path = os.path.abspath(os.path.dirname(path))

# 检测是否存在图片或poco
def exists_any(obj):

    # 若为空
    if (obj == None):
        return False

    # 若对象为图片
    if (type(obj) == Template):
        return exists(obj)

    # 若对象为poco
    if (type(obj) == UIObjectProxy):
        try:
            if (obj.exists()):
                return position_to_absolute(obj.get_position())
            else:
                return False
        except:
            return False

    # 若对象为列表
    if (type(obj) == list):
        for item in obj:
            result = exists_any(item)
            if (result != False):
                return result
        return False

def transform_xy(tuple_xy, display_info):
    x, y = tuple_xy
    x, y = XYTransformer.up_2_ori(
        (x, y),
        (display_info["width"], display_info["height"]),
        display_info["orientation"]
    )
    return x, y

def position_to_absolute(relative_position):
    '''
    根据相对位置获得绝对位置
    :param relative_position: 对象
    :return: 绝对位置
    '''
    x, y = relative_position
    width = device().display_info["width"]
    height = device().display_info["height"]
    return (x * width), (y * height)

def position_to_relative(absolute_position):
    '''
    根据绝对位置获得相对位置
    :param absolute_position: 对象
    :return: 相对位置
    '''
    x, y = absolute_position
    width = device().display_info["width"]
    height = device().display_info["height"]
    return (x / width), (y / height)

def snapshot_mag(path, name=None):
    '''
    截图
    :return:
    '''
    t = time.strftime("%Y%m%H%M%S", time.localtime())
    if name == None:
        name = t
    if name == '异常':
        name = name + t
    l_path = os.path.join(path, r'{}.jpg'.format(name))
    snapshot(filename=l_path)

def get_apk_list(apk_path_list):
    apklist = os.listdir(os.path.join(l_path, r'Game/apk'))
    for i in apklist:
        if os.path.splitext(i)[1] != ".apk":
            continue
        path = os.path.abspath(os.path.join(l_path, r'Game/apk'))
        apk_path = os.path.join(path, i)
        apk_path_list.append(apk_path)
    return apk_path_list

def install_pwd():
    sleep(10)
    pwd = {
        0: 'jkyl1209',
        1: 'cs123456'
    }
    for i in pwd:
        sleep(3)
        pwd_poc = [
            poco(nameMatches='(.*)id/et_login_passwd_edit'), # oppo
            poco(nameMatches='(.*)id/edit_Text'), # vivo
        ]
        pwd_pos = exists_any(pwd_poc)
        if pwd_pos != False:
            print('安装界面')
            for obj in pwd_poc:
                if obj.exists() != False:
                    obj.set_text(pwd[i])
                    print(pwd[i])
                    ok_b = exists_any([
                        poco(text='安装'),
                        poco(text='确定')
                    ])
                    if ok_b != False:
                        touch(ok_b)

    for j in range(0, 3):
        sleep(5)
        ok_btn = exists_any([
            poco(text='确定'),
            poco(text='安装'),
            poco(text='继续安装'),
            poco(text='完成'),
            Template(r"tpl1618986705327.png", record_pos=(0.01, 0.828), resolution=(720, 1600)),
        ])
        if ok_btn != False:
            touch(ok_btn)

def install_app(apk):
    '''
    安装App
    :return:
    '''
    dev = device()
    con = Confing()
    packagename = getAppBaseInfo(apk)
    con.write_config('JK', 'apk_path', apk)
    con.write_config('JK', 'packagename', packagename)
    p, n = os.path.split(apk)
    channel = apk.split("-")[1]
    con.write_config('JK', 'channel', channel)
    game_name = n.split("-")[0]
    con.write_config('JK', 'game', game_name)

    # 创建目录
    mkdirs_file(game_name, channel)
    input_pwd_t = threading.Thread(target=install_pwd)
    # input_pwd_t.setDaemon(True)
    input_pwd_t.start()
    try:
        dev.check_app(packagename)
        dev.uninstall_app(packagename)
        install(apk)
    except Exception as e:
        print(e)
        install(apk)

def login_wx():
    for i in range(5):
        sleep(5)
        wx_login = exists_any(poco(text='微信号/QQ号/邮箱登录'))
        if wx_login == False:
            print('111')
            continue
        user_poc = [
            poco(text='请填写微信号/QQ号/邮箱'),
        ]
        for user_obj in user_poc:
            if user_obj.exists() != False:
                user_obj.set_text('19928306334')
        pwd_poc = [
            poco(text='请填写密码'),
        ]
        for pwd_obj in pwd_poc:
            if pwd_obj.exists() != False:
                pwd_obj.set_text('qwert77889')

        login_btn = exists_any(poco(text='登录'))
        if login_btn != False:
            touch(login_btn)
        break
    sleep(3)
    slide_btn_poc = [
        Template(r"tpl1618933350010.png", record_pos=(-0.3, -0.119), resolution=(1080, 2340))
    ]
    slide_btn = exists_any(slide_btn_poc)
    if slide_btn != False:
        x = slide_btn[0]+300
        while True:
            sleep(1)
            slide_btn = exists_any(slide_btn_poc)
            if slide_btn == False:
                break
            x += 50
            swipe(slide_btn, [x, slide_btn[1]])
        sleep(3)
    p = exists_any(Template(r"tpl1618935089614.png", record_pos=(-0.016, 0.765), resolution=(1080, 2340)))
    if p != False:
        touch(p)
    sleep(1)
    nex = exists_any(Template(r"tpl1618936202002.png", record_pos=(0.0, 0.916), resolution=(1080, 2340)))
    if nex != False:
        touch(nex)
    sleep(2)
    azyz = exists_any(Template(r"tpl1618935263237.png", record_pos=(-0.001, 0.76), resolution=(1080, 2340)))
    if azyz != False:
        touch(azyz)
    sleep(1)
    yzfs = exists_any(Template(r"tpl1618935407127.png", threshold=0.9000000000000001, record_pos=(-0.291, -0.724), resolution=(1080, 2340)))
    if yzfs != False:
        touch(yzfs)
        filepath = os.path.join(path, r'wx.jpg')
        ma = exists_any(Template(r"tpl1618935926010.png", record_pos=(-0.004, 0.051), resolution=(1080, 2340)))
        if ma != False:
            sleep(3)
            snapshot(filename=filepath)
            Send_Mail()
    #TODO：扫码后的操作

def getAppBaseInfo(apk_path):
    '''
    获取apk包名，版号，内部版本号，权限信息
    :param parm_apk_path: apk文件
    :return: packagename, versionCode, versionName
    '''
    aapt_path = os.path.join(path, 'aapt.exe')
    # 使用命令获取版本信息
    get_info_command = "%s dump badging %s" % (aapt_path, apk_path)
    info_output = subprocess.Popen(get_info_command, shell=True, stdout=subprocess.PIPE)
    info = info_output.stdout.read().decode('utf8', 'ignore')

    # 通过正则匹配，获取包名，版本号，版本名称
    match = re.compile("package: name='(\S+)' versionCode='(\d+)' versionName='(\S+)'").match(info)
    if not match:
        raise Exception("{}找不到apk版号信息~".format(apk_path))

    g_name = re.search("application-label-zh:'(\S+)'", info)
    # print(g_name is None)
    if g_name is None:
        g_name = re.search("application-label-zh-CN:'(\S+)'", info)
    elif g_name is None:
        g_name = re.search("application-label-zh-Hans-CN:'(\S+)'", info)
    elif g_name is None:
        g_name = re.search("application-label-en:'(\S+)'", info)
    elif g_name is None:
        raise Exception("{}找不到apk中文名信息~".format(apk_path))
    # print(g_name.group(1))
    packagename = match.group(1)
    # versionCode = match.group(2)
    # versionName = match.group(3)
    # apk_game_name = g_name.group(1)
    return packagename

def mkdirs_file(game, channel):
    '''
    创建文件目录
    :param channel:
    :return:
    '''
    path_new = os.path.join(l_path, r'Game\{}\resultlog\{}').format(game, channel)
    image_path = os.path.join(path_new, r'image')
    recorder_path = os.path.join(path_new, r'recorder')
    err_path = os.path.join(path_new, r'err')
    for i in [path_new, image_path, recorder_path, err_path]:
        try:
            os.makedirs(i)
        except FileExistsError as fe:
            print(fe, '正在清除目录下文件...')
            del_list = os.listdir(i)
            for j in del_list:
                file_path = os.path.join(i, j)
                if os.path.isfile(file_path):
                    os.remove(file_path)

class Recorder_device():
    '''录像'''
    def __init__(self):
        self.get_devices()
        self.recorder = Recorder(ADB(serialno=self.get_devices()))
        self.name = 'test'

    def get_devices(self):
        '''
        获取设备号
        :return:
        '''
        adb = ADB()
        devicesList = adb.devices()
        return devicesList[0][0]
        # connect_device() # 连接设备

    def start_recorder(self, max_time):
        '''
        开始录像
        :param max_time: 录制时长,最大为1800,单位s
        :return:
        '''
        try:
        # str = "adb logcat -c"
        # app = device()
        # app.shell(str)
            self.recorder.start_recording(max_time=max_time)
        except:
            print("启动录像失败！")

    def stop_recorder(self, channel, filename=None):
        try:
            game_name = Confing().get_config('JK', 'game')
            self.recorder.stop_recording(is_interrupted=True)
            if filename == None:
                filename = self.name
            f_path = os.path.join(l_path, r'Game\{}\resultlog\{}\recorder\{}.mp4').format(game_name, channel, filename)
            self.recorder.pull_last_recording_file(f_path)
        except:
            pass

class Confing():
    '''配置相关操作'''
    def __init__(self):
        self.config_path = os.path.join(l_path, 'config.ini')
        self.config = configparser.ConfigParser()
        self.config.read(self.config_path, encoding="GB18030")

    def get_config(self, section, option):
        '''
        读取配置
        :param section:
        :param option:
        :return:
        '''
        data = self.config.get(section, option)
        return data

    def write_config(self, section, option, value=None):
        '''
        修改配置
        :param section:
        :param option:
        :param value:
        :return:
        '''
        self.config.set(section, option, value)
        self.config.write(open(self.config_path, 'r+', encoding='utf-8'))
