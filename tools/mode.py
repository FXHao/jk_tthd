# -*- coding: utf-8 -*-
import multiprocessing

# from airtest.core.api import *

# 设置日志输出等级
import logging
logger = logging.getLogger('airtest')
logger.setLevel(logging.FATAL)

from poco.drivers.android.uiautomation import AndroidUiautomationPoco
poco = AndroidUiautomationPoco(use_airtest_input=True, screenshot_each_action=False)

from airtest.core.settings import Settings as ST
ST.FIND_TIMEOUT_TMP = 3 # exists等待的时间